﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleShopSystem.Sample
{
    public class ShopInitSample : MonoBehaviour
    {
        [SerializeField] SimpleShopSystem.Core.SimpleLoader loader;

        async void Start()
        {
            loader.Data.Inventories.AddInventory("global", new List<Core.Item>
            {
                new Core.Item("id1","Red Potion", "200"),
                new Core.Item("id2","Blue Potion", "150"),
                new Core.Item("id3", "Yellow Box", "4000"),
                new Core.Item("id4", "Band-Aid","10"),
                new Core.Item("id5", "Lost Map", "1")
            });

            loader.Data.Inventories.AddInventory("Player", loader.Data.Inventories.GetItemFromInventory("global", "id2"));
            loader.Data.Inventories.AddInventory("Player", loader.Data.Inventories.GetItemFromInventory("global", "id2"));
            loader.Data.Inventories.AddInventory("Player", loader.Data.Inventories.GetItemFromInventory("global", "id3"));
            loader.Data.Inventories.AddInventory("Player", loader.Data.Inventories.GetItemFromInventory("global", "id1"));

            loader.Data.Shops.AddShopItem("Shop2", "ivan1", loader.Data.Inventories.GetItemFromInventory("global", "id1"));
            loader.Data.Shops.AddShopItem("Shop2", "ivan1", loader.Data.Inventories.GetItemFromInventory("global", "id2"));
            loader.Data.Shops.AddShopItem("Shop2", "ivan1", loader.Data.Inventories.GetItemFromInventory("global", "id1"));
            loader.Data.Shops.AddShopItem("Shop2", "ivan1", loader.Data.Inventories.GetItemFromInventory("global", "id3"));
            loader.Data.Shops.AddShopItem("Shop2", "ivan1", loader.Data.Inventories.GetItemFromInventory("global", "id5"));

            await loader.Save();

            await loader.Load();

            await Core.ShopProcessor.Instance.BuyFromShop("Shop2", "ivan1", "id5");
        }

    }
}