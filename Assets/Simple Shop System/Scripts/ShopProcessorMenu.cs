﻿using UnityEditor;

namespace SimpleShopSystem.Editor
{

    public class ShopProcessorMenu
    {
        [MenuItem("Simple Shop System/Initialize", false, -1)]
        static void DoInitializeMenu()
        {
            Core.ShopProcessor.Instance.DoInitialize();
        }
    }

}