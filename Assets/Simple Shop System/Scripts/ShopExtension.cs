﻿using System.Collections.Generic;

namespace SimpleShopSystem
{
    public static class ShopExtension
    {
        public static void AddItem(this List<Core.Item> item, Core.Item newItem)
        {
            item.Add(newItem);
        }

        public static Core.Item GetFromID(this List<Core.Item> item, string id)
        {
            return item.Find(x => x.id == id);
        }

        public static Core.InventoryItem GetInventoryFromID(this List<Core.InventoryItem> inventory, string id)
        {
            return inventory.Find(x => x.id == id);
        }

        public static Core.ShopItem GetShopFromID(this List<Core.ShopItem> shops, string id)
        {
            return shops.Find(x => x.id == id);
        }

        public static void AddInventory(this List<Core.InventoryItem> inventory, string inventoryId, List<Core.Item> items)
        {
            if (inventory.GetInventoryFromID(inventoryId) == null)
            {
                inventory.Add(new Core.InventoryItem(inventoryId));
            }

            inventory.GetInventoryFromID(inventoryId).items.AddRange(items);
        }

        public static Core.Item GetItemFromInventory(this List<Core.InventoryItem> inventories, string InventoryId, string itemId)
        {
            return inventories.GetInventoryFromID(InventoryId).items.Find(x => x.id == itemId);
        }


        public static Core.Item GetItemFromShop(this List<Core.ShopItem> shops, string shopId, string inventoryId, string itemId)
        {
            return shops.GetShopFromID(shopId).inventories.GetItemFromInventory(inventoryId, itemId);
        }

        public static void AddInventory(this List<Core.InventoryItem> inventory, string inventoryId, Core.Item item)
        {
            if (inventory.GetInventoryFromID(inventoryId) == null)
            {
                inventory.Add(new Core.InventoryItem(inventoryId));
            }

            inventory.GetInventoryFromID(inventoryId).items.Add(item);
        }


        public static void AddShopItem(this List<Core.ShopItem> shops, string shopId, string inventoryId, List<Core.Item> item)
        {
            if (shops.GetShopFromID(shopId) == null)
            {
                shops.Add(new Core.ShopItem(shopId));
            }

            shops.GetShopFromID(shopId).inventories.AddInventory(inventoryId, item);            
        }

        public static void AddShopItem(this List<Core.ShopItem> shops, string shopId, string inventoryId, Core.Item item)
        {
            if (shops.GetShopFromID(shopId) == null)
            {
                shops.Add(new Core.ShopItem(shopId));
            }

            shops.GetShopFromID(shopId).inventories.AddInventory(inventoryId, item);
        }

        public static void RemoveFromInventory(this List<Core.InventoryItem> inventories, string inventoryId, string itemId)
        {
            if (inventories.GetInventoryFromID(inventoryId) == null) return;

            inventories.GetInventoryFromID(inventoryId).items.Remove(inventories.GetInventoryFromID(inventoryId).items.GetFromID(itemId));
        }

        public static void RemoveFromInventory(this List<Core.InventoryItem> inventories, string inventoryId, Core.Item item)
        {
            if (inventories.GetInventoryFromID(inventoryId) == null) return;

            inventories.GetInventoryFromID(inventoryId).items.Remove(item);
        }
    }



}