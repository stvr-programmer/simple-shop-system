﻿using System.Collections;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;

namespace SimpleShopSystem
{

    namespace Core
    {

        public class ShopProcessor
        {
            private const string creationPath = "Assets/Resources/ShopSettingData.asset";
            private ShopSettingData setting;
            private UnitOfWork uow;
            public ShopSettingData Setting
            {
                get
                {
                    if (setting == null)
                        setting = Resources.Load<ShopSettingData>("ShopSettingData");

#if UNITY_EDITOR

                    if (setting == null)
                        DoInitialize();
#endif

                    return setting;
                }
            }

            private static ShopProcessor instance;
            public static ShopProcessor Instance
            {
                get
                {
                    if (instance == null)
                        instance = new ShopProcessor();
                    return instance;
                }
            }

            private static SimpleLoader loader;

            public UnitOfWork UoW
            {
                get
                {
                    if (uow == null)
                        uow = Object.FindObjectOfType<UnitOfWork>();
                    return uow;
                }
            }
            //Core.Item _item;

            public static SimpleLoader Loader
            {
                get
                {
                    if (loader == null)
                        loader = Object.FindObjectOfType<SimpleLoader>();
                    return loader;
                }
            }

            #region Editor

#if UNITY_EDITOR

            public void DoInitialize()
            {
                ShopSettingData _setting = ScriptableObject.CreateInstance<ShopSettingData>();
                AssetDatabase.CreateAsset(_setting, creationPath);
                instance.setting = _setting;
            }


#endif

            #endregion

            public async Task BuyFromShop(string shopId, string inventoryId, string itemId)
            {
                //Debug.Log(Loader.Data.Shops.GetShopFromID(shopId));
                Item item = Loader.Data.Shops.GetItemFromShop(shopId, inventoryId, itemId);
                Loader.Data.Inventories.AddInventory("Player", item);
                Loader.Data.Shops.GetShopFromID(shopId).inventories.RemoveFromInventory(inventoryId, itemId);
                await Loader.Save();
            }

        }
        public enum ShopSystemType
        {
            UseRepositoryPattern = 0,
            Default = 1
        }
    }



}