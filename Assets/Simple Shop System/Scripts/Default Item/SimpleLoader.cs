﻿using System.IO;
using System.Threading.Tasks;
using UnityEngine;

namespace SimpleShopSystem.Core
{
    public class SimpleLoader : MonoBehaviour
    {
        DefaultGameData data = new DefaultGameData();

        [SerializeField] protected string folderName;
        [SerializeField] protected string fileName;

        public DefaultGameData Data => data;

        string fullPath => Path.Combine(Application.streamingAssetsPath, folderName, fileName);
        string folderPath => Path.Combine(Application.streamingAssetsPath, folderName);

        string currentDir;
        string pathCurDir;
        DirectoryInfo _info;
        StreamWriter writer;
        StreamReader reader;

        string json;

        public async Task Load()
        {
            await ValidateFolder();

            if (!File.Exists(fullPath)) return;
            reader = new StreamReader(fullPath);
            json = await reader.ReadToEndAsync();
            JsonUtility.FromJsonOverwrite(json, data);
            reader.Close();
        }

        public async Task Save()
        {
            await ValidateFolder();
            json = JsonUtility.ToJson(data, true);
            using (writer = new StreamWriter(fullPath))
            {
                await writer.WriteAsync(json);
                await writer.FlushAsync();

                writer.Close();
            }

            await Task.Delay(10);
        }


        protected async Task ValidateFolder()
        {
            if (File.Exists(fullPath)) return;

            currentDir = Directory.GetCurrentDirectory();
            pathCurDir = Path.Combine(currentDir, Application.streamingAssetsPath);

            if (!Directory.Exists(pathCurDir))
            {
                _info = Directory.CreateDirectory(pathCurDir);
            }

            if (!Directory.Exists(folderPath))
            {
                _info = Directory.CreateDirectory(folderPath);
            }

            await Task.Delay(10);
        }
    }

}