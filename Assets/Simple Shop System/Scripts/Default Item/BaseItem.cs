﻿using System.Collections;
using System.Collections.Generic;

namespace SimpleShopSystem.Core
{
    [System.Serializable]
    public abstract class BaseItem
    {
        public string id;

        public BaseItem(string id)
        {
            this.id = id;
        }
    }

    [System.Serializable]
    public class Item : BaseItem
    {
        public string name;
        public string cost;

        public Item(string id, string name, string cost) : base(id)
        {
            this.name = name;
            this.cost = cost;
        }
    }


    [System.Serializable]
    public class InventoryItem : BaseItem
    {
        public List<Item> items;

        public InventoryItem(string id) : base(id)
        {
            items = new List<Item>();
        }
    }


    [System.Serializable]
    public class ShopItem : BaseItem
    {
        public List<InventoryItem> inventories;

        public ShopItem(string id) : base(id)
        {
            inventories = new List<InventoryItem>();
        }
    }

}