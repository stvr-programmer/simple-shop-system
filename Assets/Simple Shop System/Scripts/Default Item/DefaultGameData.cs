﻿using System.Collections.Generic;

namespace SimpleShopSystem.Core
{
    [System.Serializable]
    public class DefaultGameData
    {
        public List<InventoryItem> Inventories;
        public List<ShopItem> Shops;

        public DefaultGameData()
        {
            Inventories = new List<InventoryItem>();
            Shops = new List<ShopItem>();
        }

    }

}