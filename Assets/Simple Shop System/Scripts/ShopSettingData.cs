﻿using UnityEngine;

namespace SimpleShopSystem
{
    namespace Core
    {
        public class ShopSettingData : ScriptableObject
        {
            public ShopSystemType SystemType;

            public string DefaultShopSceneName = "Shop_Default";
            public string RepositoryShopSceneName = "Shop_Repository";
        }
    }



}