﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Will save converted csv file to repos of repository pattern.
/// </summary>
///

public class CsvId
{
    public const string RW_SLOT_NAME = "SlotName";
    public const string RW_ITEM_ID_REWARD = "ItemReward";
    public const string RW_SLOT_COND_FULFILL = "RewardCondition";
    public const string RW_SLOT_COND_STATEMENT = "rewardStatement";
    public const string ITEM_ID = "itemId";
    public const string ITEM_VALUE = "itemValue";
    public const string ITEM_NAME = "itemName";

    public const string COND_MISSION_1 = "mission1";

}

public class CSVDataExample : MonoBehaviour
{
    [SerializeField] CSVDataLoader loader;
    [SerializeField] UnitOfWork UoW;

    List<ItemData> _temps = new List<ItemData>();

    public string InventoryName;
    
    private async void Start()
    {
        await loader.LoadCSVFile();
        await Task.Delay(1);
        UoW.InventoryRepository.Add(new InventoryData(InventoryName, ConvertItemToList<ItemData>(loader.Data)));

        UoW.Save();
    }

    protected List<T> ConvertItemToList<T>(List<Dictionary<string, object>> _data) where T : ItemData, new()
    {
        List<T> _tempItems = new List<T>();

        for (int i = 0; i < _data.Count; i++)
        {
            T _tempItem = new T();
            _tempItem.Id = _data[i][CsvId.ITEM_ID].ToString();
            _tempItem.Values = float.Parse(_data[i][CsvId.ITEM_VALUE].ToString());
            _tempItem.ItemName = _data[i][CsvId.ITEM_NAME].ToString();

            _tempItems.Add(_tempItem);

        }

        return _tempItems;
    }
}

