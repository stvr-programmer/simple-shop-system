﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class SaveDataToJSONSample : MonoBehaviour
{
    [SerializeField] JSONDataLoader loader;
    [SerializeField] UnitOfWork UoW;

    //save dummy data to json file.
    async void Start()
    {
        await InitializeData();
        UoW.Save();
        UoW.Load();
    }


    async Task InitializeData()
    {
        UoW.InventoryRepository.Add(new InventoryData("myInventory", new List<ItemData>
        {
            new ItemData("itemId1",20f,"Potion"),
            new ItemData("ItemId2", 30f,"Dagger"),
            new ItemData("itemId3", 40f, "Knife")
        }));

        UoW.ShopRepository.Add(new ShopData("myShop", UoW.InventoryRepository.Get(x => x.Id == "myInventory")));


        await Task.Delay(20);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var item = UoW.InventoryRepository.GetItemInInventory("myInventory", "itemId1");
            item.ItemName = "Blue Potion";

            UoW.Save();
        }
    }
}
