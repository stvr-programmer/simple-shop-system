﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public interface IRepository<T> where T : class
{
    T Get(Func<T, bool> predicate);
    IEnumerable<T> GetAll();

    T Find(Predicate<T> predicate);

    void Add(T entity);
    void AddRange(IEnumerable<T> entities);

    void Remove(T entity);
    void RemoveRange(int index, int count);
}
