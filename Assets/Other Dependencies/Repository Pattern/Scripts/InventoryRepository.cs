﻿public class InventoryRepository : Repository<InventoryData>
{
    InventoryData _tempInventory;
    public ItemData GetItemInInventory(string inventoryId, string itemId)
    {
        _tempInventory = Get(x => x.Id == inventoryId);
        return _tempInventory.GetItem(itemId);
    }

    public void AddItemInInventory(string inventoryId, ItemData item)
    {
        _tempInventory = Get(x => x.Id == inventoryId);
        _tempInventory.Items.Add(item);
    }
}
