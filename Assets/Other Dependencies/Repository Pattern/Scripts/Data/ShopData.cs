﻿using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class ShopData : BaseData
{
    public List<InventoryData> Inventory = new List<InventoryData>();

    ItemData _tempItem;
    InventoryData _tempInventory;

    public ShopData(string shopId, List<InventoryData> inventory)
    {
        Id = shopId;
        Inventory = inventory;
    }

    public ShopData(string shopId, InventoryData inventory)
    {
        Id = shopId;
        Inventory.Add(inventory);

    }

    public ItemData GetItemFromInventory(string id, string inventoryId)
    {
        _tempInventory = Inventory.FirstOrDefault(x => x.Id == inventoryId);
        _tempItem = _tempInventory.Items.FirstOrDefault(x => x.Id == id);
        return _tempItem;
    }
}