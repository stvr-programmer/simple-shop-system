﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;

//not to be confused with repository pattern one.
public class CSVDataLoader : MonoBehaviour
{
    public string folderName;
    public string fileName;

    public

    string fullPath => Path.Combine(Application.streamingAssetsPath, folderName, fileName);
    string folderPath => Path.Combine(Application.streamingAssetsPath, folderName);

    private List<Dictionary<string, object>> _data;

    public List<Dictionary<string, object>> Data => _data;
    private string currentDir;
    private string pathCurDir;
    private DirectoryInfo _info;


    public async Task LoadCSVFile()
    {
        await ValidateFolder();
        _data = CSVReader.ReadStreamingAssets(fullPath);
    }

    protected async Task ValidateFolder()
    {
        if (File.Exists(fullPath)) return;

        currentDir = Directory.GetCurrentDirectory();
        pathCurDir = Path.Combine(currentDir, Application.streamingAssetsPath);

        if (!Directory.Exists(pathCurDir))
        {
            _info = Directory.CreateDirectory(pathCurDir);
        }

        if (!Directory.Exists(folderPath))
        {
            _info = Directory.CreateDirectory(folderPath);
        }

        await Task.Delay(10);
    }

}


[System.Serializable]
public struct CSVDataRead
{
    public string id;
    public string idType;

    public CSVDataRead(string id, string idType)
    {
        this.id = id;
        this.idType = idType;
    }
}