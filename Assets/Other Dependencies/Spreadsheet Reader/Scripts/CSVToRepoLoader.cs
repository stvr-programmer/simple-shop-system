﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CSVToRepoLoader : BaseCSVToRepositoryLoader<CSVToRepoLoader>
{
    InventoryData _tempInventory;
    List<ShopData> _tempShops;
    List<ItemData> _items;
    List<InventoryData> _tempInventories;

    private async void Start()
    {
        for (int i = 0; i < CSVLoaders.Count; i++)
        {
            await CSVLoaders[i].CSVLoader.LoadCSVFile();
            SetToRepository(CSVLoaders[i].CSVLoader.Data, CSVLoaders[i].LoadToRepository, CSVLoaders[i].Keys);
        }

        UoW.Save();
    }

    public override void SetToRepository(List<Dictionary<string, object>> Data, LoadTo loadToRepo, List<string> keys)
    {
        switch (loadToRepo)
        {
            case LoadTo.GlobalInventory:
                LoadToGlobalInventory(Data, keys);
                break;

            case LoadTo.Inventory:
                LoadToInventory(Data, keys);
                break;

            case LoadTo.Shop:
                LoadToShop(Data, keys);
                break;
        }

    }

    private void LoadToGlobalInventory(List<Dictionary<string, object>> Data, List<string> keys)
    {
        _items = new List<ItemData>();
        for (int i = 0; i < Data.Count; i++)
        {
            _items.Add(new ItemData(ReadObjectAs<string>(Data[i][keys[0]]), ReadObjectAs<float>(Data[i][keys[1]]), ReadObjectAs<string>(Data[i][keys[2]])));
        }

        _tempInventory = new InventoryData("global", _items);

        UoW.InventoryRepository.Add(_tempInventory);
    }

    private void LoadToInventory(List<Dictionary<string, object>> Data, List<string> keys)
    {
        _tempInventories = new List<InventoryData>();

        for (int i = 0; i < Data.Count; i++)
        {
            _tempInventories.Add(new InventoryData(ReadObjectAs<string>(Data[i][keys[0]]), UoW.InventoryRepository.GetItemInInventory("global", ReadObjectAs<string>(Data[i][keys[1]]))));
        }

        for (int i = _tempInventories.Count - 1; i >= 0; i--)
        {
            if (UoW.InventoryRepository.Get(x => x.Id == _tempInventories[i].Id) != null)
            {
                UoW.InventoryRepository.Get(x => x.Id == _tempInventories[i].Id).Items.AddRange(_tempInventories[i].Items);
            }
            else
                UoW.InventoryRepository.Add(_tempInventories[i]);
        }
    }

    private void LoadToShop(List<Dictionary<string, object>> Data, List<string> keys)
    {
        _tempShops = new List<ShopData>();

        for (int i = 0; i < Data.Count; i++)
        {
            _tempShops.Add(new ShopData(ReadObjectAs<string>(Data[i][keys[0]]), UoW.InventoryRepository.Get(x => x.Id == ReadObjectAs<string>(Data[i][keys[1]]))));
        }

        for (int i = 0; i < _tempShops.Count; i++)
        {
            if (UoW.ShopRepository.Get(x => x.Id == _tempShops[i].Id) != null)
            {
                UoW.ShopRepository.Get(x => x.Id == _tempShops[i].Id).Inventory.Add(UoW.InventoryRepository.Get(x => x.Id == ReadObjectAs<string>(Data[i][keys[1]])));
            }
            else
                UoW.ShopRepository.Add(_tempShops[i]);
        }
    }
}