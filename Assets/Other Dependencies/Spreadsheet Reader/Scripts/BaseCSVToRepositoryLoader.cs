﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCSVToRepositoryLoader<T2> : MonoBehaviour
{
    [SerializeField] protected UnitOfWork UoW;
    [SerializeField] protected List<CSVDataLoaderStruct> CSVLoaders;

    public T ReadObjectAs<T>(object objs)
    {
        if (objs is T)
        {
            return (T)objs;
        }

        try
        {
            return (T)Convert.ChangeType(objs, typeof(T));
        }
        catch (System.Exception)
        {
            Debug.Log("unable to Convert");
        }

        return default(T);
    }

    public abstract void SetToRepository(List<Dictionary<string, object>> Data, LoadTo loadToRepo, List<string> keys);

}

[System.Serializable]
public struct CSVDataLoaderStruct
{
    public CSVDataLoader CSVLoader;
    public LoadTo LoadToRepository;
    public List<string> Keys;

    public CSVDataLoaderStruct(CSVDataLoader cSVLoader, LoadTo loadToRepository, List<string> Keys)
    {
        CSVLoader = cSVLoader;
        LoadToRepository = loadToRepository;
        this.Keys = Keys;
    }
}

public enum LoadTo
{
    Inventory = 0,
    Shop = 1,
    GlobalInventory = 2 //a collection of item inside that can be reached everywhere.
}
